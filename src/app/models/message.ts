import { Device } from "src/app/models/device";

export interface Message {
  messageId: string,
  sender: Device;
  receiver: Device;
  body: string;
  sentDate: number;
  receiveDate: number;
}
