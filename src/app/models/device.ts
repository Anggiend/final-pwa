export interface Device {
  id: number;
  token: string;
}
