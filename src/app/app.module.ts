import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';

import { PushService } from './services/push.service';
import { DeviceService } from 'src/app/services/device.service';
import { MessageService } from 'src/app/services/message.service';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('combined-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    PushService,
    DeviceService,
    MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
