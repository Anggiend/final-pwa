import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase';
import { Device } from 'src/app/models/device';
import { DeviceService } from 'src/app/services/device.service';
import { MessageService } from 'src/app/services/message.service';
import { Message } from '../models/message';

/**
 * @title Basic cards
 */
@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit{
  device: Device;
  messaging: any;
  messages: Message [];
  // tokens: FirebaseListObservable<any[]>

  constructor(
    private router: Router,
    private deviceService: DeviceService,
    private messageService: MessageService) {
    // Declaring the property value of messaging
    this.messaging = firebase.messaging();

    // Check for token refresh
    this.messaging.onTokenRefresh(function () {
      this.messaging.getToken()
        .then(function (refreshedToken) {
          console.log('Token refreshed.');
        })
        .catch(function (err) {
          console.log('Unable to retrieve refreshed token ', err);
        });
    });
  }

  async ngOnInit() {
    const self = this;
    this.device = JSON.parse(localStorage.getItem("device"));
    this.messages =  await this.getReceivedMessage();
    console.log(this.messaging);
    navigator.serviceWorker.getRegistration().then(registration => {
      //Mengubah default service worker menjadi combined worker
      console.log("percobaan: " + registration);
      this.messaging.useServiceWorker(registration);
      
      this.messaging.requestPermission()
      .then(function () {
        console.log('Notification permission granted.');
        self.messaging.getToken()
          .then(function (currentToken) {
            if (currentToken) {
              console.log("token: " + currentToken);
              // Set a timeout so as to enable all the data to be loaded
              setTimeout(async () => { 
                if(self.device == null){ //cek token apakah sudah terdaftar atau blm?
                  self.device = <Device>{};
                  
                  const res = await self.registerDevice(currentToken)
                  self.device.id = res.id;
                  self.device.token = res.token;
                  localStorage.setItem('device', JSON.stringify(self.device));
                }
              }, 6500);
            } else {
              // Show permission request.
              console.log('No Instance ID token available. Request permission to generate one.');
            }
          })
          .catch(function (err) {
            console.log('An error occurred while retrieving token.', err);
          });
      })
      .catch(function (err) {
        console.log('Unable to get permission to notify. ', err);
      })

      // Handle incoming messages. Called when:
      // - a message is received while the app has focus
      // - the user clicks on an app notification created by a sevice worker `messaging.setBackgroundMessageHandler` handler.
      this.messaging.onMessage(function (payload) {
        var receiveDate = Math.floor(Date.now());
        var data = payload.data;
        
        var notificationTitle = payload.notification.title;
        var notificationOptions = {
          body: payload.notification.body,
          image: payload.data.image
        };

        registration.showNotification(notificationTitle, notificationOptions);

        self.messageService.updateReceiveDate(data.messageId, receiveDate)
        .subscribe(res => console.log(res));
      });
    })
  }

  registerDevice(token: string): Promise<Device> {
    const self = this;
    return new Promise(function(resolve, reject){
      self.deviceService.registerDevice(token)
      .subscribe((res: Device) => resolve(res));
    });
  }

  getReceivedMessage(): Promise<Message []>{
    const self = this;
    return new Promise(function(resolve, reject){
      if(self.device == null){ resolve([]);}
      self.deviceService.getReceivedMessage(self.device.id)
      .subscribe((res: Message[]) => resolve(res));
    });
  }
}