import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageService } from 'src/app/services/message.service';

@Injectable()
export class PushService {
  data: any = {
    notification: {
      title: "Notification",
      body: "",
      click_action: "http://localhost:4200/#/home"
    },
    data: {
    },
    to: ""
  }

  constructor(private http: Http, private messageService: MessageService) { }

  // Providers function that triggers a push operation
  pushMessage(message) {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'key=AAAAbroC_ds:APA91bGsNms8pECjNt0Mo-XeoM8VpymfpgKTsMno3tUcF8y1kg9m5I-7Vgh4MR95ae3wkDZH0TJu_9ru2_kFrgBRYYxBz-MF-b7cRquFmO5qe8OKVDV8hjz9Gfqqr48YbEJsvxFW7tl2'})
    let options = new RequestOptions({ headers: headers })
    

    this.data.data = {messageId: message.messageId};
    this.data.notification.body = message.body;
    this.data.to = message.receiver.token;
    var self = this;
    
    return this.http.post('https://fcm.googleapis.com/fcm/send', this.data, options).pipe(
        map(data => data.json()));
  }
  
}
