import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response} from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class DeviceService {
  base_url: string = 'http://localhost:8080';

  constructor(private http: Http){}

  registerDevice(token: string){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers });
    
    return this.http.post(this.base_url + '/device/', {token: token}, options).pipe(
    map((response: Response) => {
      return response.json();
    }));
  }

  getReceivedMessage(id: number){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers });
    
    return this.http.get(this.base_url + '/device/' + id + '/receivedMessage', options).pipe(
    map((response: Response) => {
      return response.json();
    }));
  }
}