import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, RequestOptions, Headers, Response} from '@angular/http';

@Injectable()
export class MessageService {
  base_url: string = 'http://localhost:8080';

  constructor(private http: Http){}


  updateReceiveDate(messageId: string, receiveDate: number){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers });
    
    let data = {
      receive_date: receiveDate
    }
    const url = this.base_url + '/message/' + messageId + '/receiveDate';
   
    return this.http.put(url, data, options).pipe(
    map((response: Response) => {
      return response.json();
    }));
  }
}