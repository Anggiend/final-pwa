import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { firebaseConfig } from './../environments/firebase.config';
firebase.initializeApp(firebaseConfig);

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  title = 'PWA-client';
}
